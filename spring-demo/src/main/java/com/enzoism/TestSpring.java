package com.enzoism;

import com.enzoism.config.DemoConfig;
import com.enzoism.model.UserModel;
import com.enzoism.service.UserService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class TestSpring {
	public static void main(String[] args) {
		// 1）基于标注-进行对象的获取
		AnnotationConfigApplicationContext ac  = new AnnotationConfigApplicationContext(DemoConfig.class);
		UserService userService = ac.getBean(UserService.class);
		userService.getAll();


		// 2）基于配置-进行对象获取
		ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("springContext.xml");
		UserModel bean = classPathXmlApplicationContext.getBean("userModel", UserModel.class);
		System.out.println(bean.toString());

	}
}
