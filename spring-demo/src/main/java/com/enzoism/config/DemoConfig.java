package com.enzoism.config;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.ComponentScan;

@Configurable
@ComponentScan(value = "com.enzoism")
public class DemoConfig {

}
